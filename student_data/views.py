import requests
from django.contrib import messages
from django.shortcuts import render, redirect
# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from student_data.models import User


def get_data(request):
    if request.method == "POST":
        rfid = request.POST.get('rfid', None)
        if rfid:
            roll_no = request.POST.get('rollno', None)
            print(roll_no)
            if roll_no:
                try:
                    user = User.objects.get(new_rollno=roll_no)
                    store = store_rfid(rfid, user.new_rollno)
                    messages.success(request, 'Student data recorded!' + user.new_rollno)
                    return redirect('get_data')
                except User.DoesNotExist:
                    messages.warning(request, 'Returned no student')
        user = None
        roll_no = request.POST.get('rollno', None)
        if roll_no:
            try:
                user = User.objects.get(new_rollno=roll_no)
                return render(request, 'student_data/submit_data.html', {'user': user})
            except User.DoesNotExist:
                messages.warning(request, 'Returned no student')
        return render(request, 'student_data/get_data.html', {'user': user})

    return render(request, 'student_data/get_data.html')


def store():
    f = open('error1.txt', 'w')
    json_data = requests.get(
        'http://10.1.68.54/ClientAPI/GetAllStudents?APIKey=321D44719B36C171B7189D6A6DB83&input=12').json()
    for j in json_data:
        if j['Phone_no'] == "":
            phone = None
        elif '-' in str(j['Phone_no']):
            phone = None
        else:
            phone = j['Phone_no']
        if j['Parent_phone_no'] == "":
            parent_phone = None
        elif '-' in str(j['Parent_phone_no']):
            parent_phone = None
        else:
            parent_phone = j["Parent_phone_no"]
        try:
            user = User.objects.create(username=j['RollNo'].upper(), first_name=j['Name'], email=j['Email'],
                                       old_rollno=j['RollNo'].upper(), new_rollno=j['RollNo'].upper(), mobile_no=phone,
                                       batch=j['Batch_year'], department=j['Department'],
                                       entry_type=j['Lateral_Regular'],
                                       parent_phone=parent_phone,
                                       stay=j['Stay'])
            print('User Saved ' + user.new_rollno)
        except Exception, e:
            try:
                user = User.objects.get(username=j['RollNo'].upper())
                user.first_name = j['Name']
                user.email = j['Email']
                user.mobile_no = phone
                user.rollno = j['RollNo'].upper()
                user.batch = j['Batch_year']
                user.department = j['Department']
                user.entry_type = j['Lateral_Regular']
                user.parent_phone = parent_phone
                user.stay = j['Stay']
                user.save()
                print ('User Updated ' + user.new_rollno)
            except Exception, e:
                f.write(j['RollNo'] + "\n")

    f.close()


def store_rfid(rfid, user):
    user = User.objects.get(new_rollno=user)
    user.rfidvalue = rfid
    user.save()
    return True


class GetData(APIView):
    def get(self, request):
        roll_no = request.GET.get('roll_no', None)
        email = request.GET.get('email', None)
        if roll_no:
            try:
                user = User.objects.get(new_rollno=roll_no)
                data = {'roll_no': roll_no,
                        'mobile_no': user.mobile_no,
                        'rfidvalue': user.rfidvalue,
                        'parent_phone': user.parent_phone,
                        'department': user.department,
                        'batch': user.batch,
                        'entry_type': user.entry_type,
                        'stay': user.stay,
                        'email': user.email,
                        'first_name': user.first_name
                        }
                return Response(data)
            except Exception, e:
                return Response({'error': 'Roll Number Does not Exist'})
        if email:
            try:
                user = User.objects.get(email=email)
                data = {'roll_no': user.new_rollno,
                        'mobile_no': user.mobile_no,
                        'rfidvalue': user.rfidvalue,
                        'parent_phone': user.parent_phone,
                        'department': user.department,
                        'batch': user.batch,
                        'entry_type': user.entry_type,
                        'stay': user.stay,
                        'email': user.email,
                        'first_name': user.first_name
                        }
                return Response(data)
            except Exception, e:
                return Response({'error': 'Email Does not Exist'}, status=400)
        else:
            # datas = []
            # for user in User.objects.all():
            # data = {'roll_no':user.new_rollno,
            #              'mobile_no':user.mobile_no,
            #              'rfidvalue':user.rfidvalue,
            #              'parent_phone':user.parent_phone,
            #              'department':user.department,
            #              'batch':user.batch,
            #              'entry_type':user.entry_type,
            #              'stay':user.stay,
            #              'email':user.email,
            #              'first_name':user.first_name
            #              }
            # datas.append(data)
            # return Response(datas)
            return Response({'error': 'Roll Number and Email Parameter Missing'})
