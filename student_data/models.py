from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.

class User(AbstractUser):
    old_rollno = models.CharField(max_length=20)
    new_rollno = models.CharField(max_length=20)
    mobile_no = models.BigIntegerField(null=True)
    rfidvalue = models.CharField(max_length=8)
    user_image = models.ImageField(upload_to='user_images/')
    parent_phone = models.BigIntegerField(null=True)
    department = models.CharField(max_length=255,null=True)
    batch = models.CharField(max_length=255,null=True)
    entry_type = models.CharField(max_length=255,null=True)
    stay = models.CharField(max_length=255,null=True)
    email = models.EmailField(_('email address'), blank=True)
    first_name = models.CharField(_('first name'), max_length=255, blank=True,null=True)



    class Meta:
        db_table = "Users"


